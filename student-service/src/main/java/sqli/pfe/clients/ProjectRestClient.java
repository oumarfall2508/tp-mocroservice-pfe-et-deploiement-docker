package sqli.pfe.clients;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sqli.pfe.modeles.Project;

import java.util.List;

@FeignClient(name = "PROJECT-SERVICE")
public interface ProjectRestClient {
    @GetMapping("/projects/{id}")
    @CircuitBreaker(name = "projectService", fallbackMethod = "getDefaultProject")
    Project findProjectById(@PathVariable String id);

    @CircuitBreaker(name = "projectService", fallbackMethod = "getDefaultAllProject")
    @GetMapping("/projects")
    List<Project> allProjects();

    default Project getDefaultProject(String id, Exception exception){
        Project project = new Project();
        project.setProjectId(id);
        project.setProjectLibelle("Not valable");
        return project;
    }

    default List<Project> getDefaultAllProject(Exception e){
        return List.of();
    }
}
