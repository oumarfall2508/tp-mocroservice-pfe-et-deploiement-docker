package sqli.pfe;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import sqli.pfe.Config.GlobalConfig;
import sqli.pfe.entities.Student;
import sqli.pfe.repositories.StudentRepository;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties(GlobalConfig.class)
public class StudentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(StudentRepository studentRepository){
		return args -> {
			studentRepository.save(Student.builder()
					.firstName("Oumar")
					.lastName("Sarr")
					.email("oumar.sarr@pfe.sqli")
					.projectId("pr1")
					.build());

			studentRepository.save(Student.builder()
					.firstName("Mohammed")
					.lastName("Lo")
					.email("mohammed.lo@pfe.sqli")
					.projectId("pr2")
					.build());

		};
	}
}
