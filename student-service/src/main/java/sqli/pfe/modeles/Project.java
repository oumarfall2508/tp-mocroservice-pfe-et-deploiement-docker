package sqli.pfe.modeles;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter @Setter @ToString
public class Project {
    private String projectId;
    private String projectLibelle;
    private LocalDate dateDebut;

    private LocalDate dateFin;
}
