package sqli.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sqli.pfe.entities.Student;

public interface StudentRepository extends JpaRepository<Student,Long> {
}
