package sqli.pfe.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import sqli.pfe.clients.ProjectRestClient;
import sqli.pfe.entities.Student;
import sqli.pfe.modeles.Project;
import sqli.pfe.repositories.StudentRepository;

import java.util.List;

@RestController
public class StudentRestController {
    private StudentRepository studentRepository;
    private ProjectRestClient projectRestClient;

    public StudentRestController(StudentRepository studentRepository, ProjectRestClient projectRestClient) {
        this.studentRepository = studentRepository;
        this.projectRestClient = projectRestClient;
    }

    @GetMapping("/students")
    public List<Student> getListStudents(){
        return studentRepository.findAll();
    }

    @GetMapping("/students/{id}")
    public Student getStudentById(@PathVariable Long id){
        Student student = studentRepository.findById(id).get();
        Project project = projectRestClient.findProjectById(student.getProjectId());
        student.setProject(project);
        return student;
    }
}
