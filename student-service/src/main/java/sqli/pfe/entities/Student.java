package sqli.pfe.entities;

import jakarta.persistence.*;
import lombok.*;
import sqli.pfe.modeles.Project;

@Entity
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @Builder
public class Student {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    @Transient
    private Project project;
    private String projectId;
}
