package sqli.pfe;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import sqli.pfe.entities.Project;
import sqli.pfe.repositories.ProjectRepository;

import java.time.LocalDate;

@SpringBootApplication
public class ProjectServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(ProjectRepository projectRepository){
		return args -> {
			projectRepository.save(Project.builder()
					.projectId("pr1")
					.projectLibelle("projet 2")
					.dateDebut(LocalDate.now())
					.build());

			projectRepository.save(Project.builder()
					.projectId("pr2")
					.projectLibelle("projet 2")
					.dateDebut(LocalDate.now())
					.build());

		};
	}
}
