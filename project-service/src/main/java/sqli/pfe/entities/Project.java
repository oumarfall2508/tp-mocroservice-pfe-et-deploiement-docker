package sqli.pfe.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @Builder
public class Project {
    @Id
    private String projectId;
    private String projectLibelle;
    private LocalDate dateDebut;
    
    private LocalDate dateFin;


}
