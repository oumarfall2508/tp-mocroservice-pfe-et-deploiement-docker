package sqli.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sqli.pfe.entities.Project;

public interface ProjectRepository extends JpaRepository<Project,String> {
}
