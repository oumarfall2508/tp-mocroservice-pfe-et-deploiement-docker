package sqli.pfe.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import sqli.pfe.entities.Project;
import sqli.pfe.repositories.ProjectRepository;

import java.util.List;

@RestController
public class ProjectRestController {
    private ProjectRepository projectRepository;

    public ProjectRestController(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @GetMapping("/projects")
    public List<Project> getListProjects(){
        return projectRepository.findAll();
    }

    @GetMapping("/projects/{id}")
    public Project getProjectById(@PathVariable String id){
        return projectRepository.findById(id).get();
    }
}
